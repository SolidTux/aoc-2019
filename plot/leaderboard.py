#!/usr/bin/env python3

import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd
import json
import sys

latest = 9
colors = [f'C{i}' for i in range(8)]

fn = sys.argv[1]

with open(fn, 'r') as f:
    data = json.load(f)

star_data = {}
names = []
for (i, (m, md)) in enumerate(data['members'].items()):
    name = str(md['name'])
    total = md['local_score']
    names.append((name, total))
    days = md['completion_day_level']
    for (day, dd) in days.items():
        for (part, d) in dd.items():
            time = datetime.utcfromtimestamp(int(d['get_star_ts']))
            star = f'{int(day):02}/{part}'
            if not star in star_data.keys():
                star_data[star] = []
            star_data[star].append((i, time))

stars = list(star_data.keys())
stars.sort()
df = pd.DataFrame(columns=['Name'] + stars + ['Total'])
for (i, (n, t)) in enumerate(names):
    df.loc[i] = [n] + [0] * len(stars) + [t]

for (k, v) in star_data.items():
    v.sort(key=lambda x: x[1])
    order = list(map(lambda y: y[0], v))
    for i in range(len(order)):
        df.loc[order[i]][k] = len(names) - i

for i in range(len(names)):
    x = []
    y = []
    yp = []
    count = 0
    for j in range(len(stars)):
        s = stars[j]
        if j > 0:
            x.append(s)
            y.append(count)
        count += df.loc[i][s]
        yp.append(count)
        x.append(s)
        y.append(count)
    plt.plot(x, y, ':', lw=0.5, color=colors[i])
    plt.plot(stars, yp, '+', ms=10, label=names[i][0], color=colors[i])
print(df)
plt.legend()
plt.tight_layout()
plt.show()

