#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('target/aoc/aoc-autobuild/day22.txt')
plt.plot(np.correlate(data, data, mode='full'), '-.', ms=1, lw=0.5)
plt.tight_layout()
plt.show()
