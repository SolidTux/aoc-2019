use aoc_runner_derive::{aoc, aoc_generator};
use num_bigint::BigInt;
use std::{collections::LinkedList, fs::File, io::Write, num::ParseIntError, str::FromStr};
use Operation::*;

#[derive(Debug)]
enum Operation {
    NewStack,
    Cut(i128),
    Increment(i128),
}

impl Operation {
    fn apply(&self, list: &mut LinkedList<usize>) {
        let l = list.len() as i128;
        match self {
            NewStack => *list = list.iter().rev().copied().collect(),
            Cut(mut n) => {
                if n < 0 {
                    n += l;
                }
                let mut l2 = list.split_off(n as usize);
                while let Some(x) = l2.pop_back() {
                    list.push_front(x);
                }
            }
            Increment(n) => {
                let mut new = vec![0; l as usize];
                let mut iter = list.iter().copied();
                for i in 0..l {
                    new[(n * (i as i128) % l) as usize] = iter.next().unwrap();
                }
                *list = new.into_iter().collect();
            }
        }
    }

    fn apply_pos(&self, p: i128, l: i128) -> i128 {
        match self {
            NewStack => l - p - 1,
            &Cut(n) => {
                let n = if n < 0 { n + l } else { n };
                if p >= n {
                    (p + l - n) % l
                } else {
                    p + (l - n)
                }
            }
            &Increment(n) => (p * n as i128) % l,
        }
    }

    fn code(&self, l: i128) -> String {
        match self {
            NewStack => format!("x = {} - x - 1;", l),
            &Cut(n) => {
                let n = if n < 0 { n + l } else { n };
                format!("x = (x + {}) % {};", l - n, l)
            }
            &Increment(n) => format!("x = x * {} % {};", n, l),
        }
    }

    fn parts(&self, l: i128) -> (i128, i128) {
        match self {
            NewStack => (-1, l - 1),
            &Cut(n) => {
                let n = if n < 0 { n + l } else { n };
                (1, l - n)
            }
            &Increment(n) => (n, 0),
        }
    }
}

impl FromStr for Operation {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("deal with") {
            let n = s[20..].parse()?;
            Ok(Increment(n))
        } else if s.starts_with("deal into") {
            Ok(NewStack)
        } else {
            let n = s[4..].parse()?;
            Ok(Cut(n))
        }
    }
}

#[aoc_generator(day22)]
fn parse(input: &str) -> Vec<Operation> {
    input.lines().map(|x| x.parse().unwrap()).collect()
}

#[aoc(day22, part1)]
fn part1(input: &[Operation]) -> usize {
    let mut stack: LinkedList<usize> = (0..10007).collect();
    for o in input {
        o.apply(&mut stack);
    }
    for o in input {
        o.apply(&mut stack);
    }
    stack
        .into_iter()
        .enumerate()
        .find(|&(_, x)| x == 2019)
        .unwrap()
        .0
}

//#[aoc(day22, part1, code)]
//fn code(input: &[Operation]) -> usize {
//let mut f = File::create("day22.rs").unwrap();
//writeln!(f, "const fn shuffle(x: i128) -> i128 {{ let mut x = x;").unwrap();
//for o in input {
//writeln!(f, "{}", o.code(119315717514047)).unwrap();
////writeln!(f, "{}", o.code(10007)).unwrap();
//}
//writeln!(f, "x }}").unwrap();
//0
//}

//include!("../target/aoc/aoc-autobuild/day22.rs");

fn pow(b: &BigInt, e: &BigInt) -> BigInt {
    println!("{}", e);
    let mut res = b.clone();
    if *e == 1.into() {
        res
    } else {
        if e % 2 == 0.into() {
            res *= b;
            let e: BigInt = e.clone() / 2;
            pow(&res, &e)
        } else {
            res *= b;
            let e: BigInt = (e.clone() - 1) / 2;
            pow(&res, &e) * b
        }
    }
}

#[aoc(day22, part2)]
fn part2(input: &[Operation]) -> BigInt {
    //let n = 101741582076661_u128.into();
    //let l = 119315717514047;
    let n = 2.into();
    let l = 10007;
    let start: BigInt = 2019.into();
    let mut mul: BigInt = 1.into();
    let mut add: BigInt = 0.into();
    for o in input {
        let (m, a) = o.parts(l);
        let m: BigInt = m.into();
        let a: BigInt = a.into();
        mul *= m.clone();
        add *= m;
        add += a;
    }
    println!("{} {}", mul, add);
    mul = (mul % l + l) % l;
    add = (add % l + l) % l;
    //let mn = mul.modpow(&n, &l.into());
    let mn = pow(&mul, &n);
    let x = (add * (mn.clone() - 1) + mn * start * (mul.clone() - 1)) / (mul - 1);
    (x % l + l) % l
}

//#[aoc(day22, part2, code)]
//fn code2(input: &[Operation]) -> i128 {
//let mut pos = 2020;
//let n: i128 = 101741582076661;
//let iters = 1_000_000_000;
//let bar = ProgressBar::new(iters).with_style(ProgressStyle::default_bar().template(
//"{elapsed_precise} {wide_bar:.yellow/orange} {pos:>5}/{len:<5} ETA: {eta_precise}",
//));
//bar.tick();
//let step = n / iters as i128;
//for _ in 0..iters {
//for _ in 0..step {
//pos = shuffle(pos);
//}
//bar.inc(1);
//}
//for _ in 0..(n - (iters as i128) * (step as i128)) {
//pos = shuffle(pos);
//}
//bar.finish();
//pos
//}
