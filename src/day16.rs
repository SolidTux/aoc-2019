use aoc_runner_derive::{aoc, aoc_generator};
use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use std::iter;

#[aoc_generator(day16)]
fn parse(input: &str) -> Vec<isize> {
    //let input = "00000020";
    //let input = "03036732577212944063491565474664";
    input
        .chars()
        .filter_map(|x| x.to_digit(10))
        .map(|x| x as isize)
        .collect()
}

#[aoc(day16, part1)]
fn part1(input: &[isize]) -> String {
    let base: [isize; 4] = [0, 1, 0, -1];
    let mut v: Vec<isize> = input.iter().map(|&x| x as isize).collect();
    for _ in 0..100 {
        v = (0..v.len())
            .map(|n| {
                v.iter()
                    .zip(
                        base.iter()
                            .map(|&x| iter::repeat(x).take(n + 1))
                            .flatten()
                            .cycle()
                            .skip(1),
                    )
                    .map(|(&a, b)| a * b)
                    .sum::<isize>()
                    .abs()
                    % 10
            })
            .collect();
    }
    v[0..8]
        .into_iter()
        .fold(String::new(), |acc, x| format!("{}{}", acc, x))
}

//#[aoc(day16, part2)]
//fn part2(input: &[isize]) -> String {
//let n = input.len();
//let mut v = Array1::from(input.to_vec());
//let mut m = Array2::zeros((n, n));
//for i in 0..n {
//for j in 0..n {
//for k in 0..10000 {
//m[[i, j]] += match ((j + 1 + k * n) / (i + 1)) % 4 {
//0 | 2 => 0,
//1 => 1,
//3 => -1,
//_ => unreachable!(),
//};
//}
//}
//}
//for _ in 0..100 {
//v = m.dot(&v).map(|x| x.abs()) % 10;
//}
//let offset = v
//.slice(s![0..7])
//.into_iter()
//.fold(String::new(), |acc, x| format!("{}{}", acc, x))
//.parse::<usize>()
//.unwrap();
//let mut res = Vec::new();
//for i in offset..(offset + 8) {
//res.push(v[i % n]);
//}
//res.into_iter()
//.fold(String::new(), |acc, x| format!("{}{}", acc, x))
//}

#[aoc(day16, part1, indices)]
fn part1_ind(input: &[isize]) -> String {
    let mut v: Vec<isize> = input
        .iter()
        .map(|&x| x as isize)
        //.cycle()
        //.take(10000 * input.len())
        .collect();
    for _ in 0..100 {
        v = (0..v.len())
            .map(|n| {
                let mut res = 0;
                for k in 0.. {
                    let a = (4 * k + 1) * (n + 1);
                    if a > v.len() {
                        break;
                    }
                    for x in 0..=n {
                        let b = a + x;
                        if b > v.len() {
                            break;
                        }
                        res += v[b - 1];
                        let c = b + 2 * (n + 1);
                        if c <= v.len() {
                            res -= v[c - 1];
                        }
                    }
                }
                res.abs() % 10
            })
            .collect();
    }
    v[0..8]
        .into_iter()
        .fold(String::new(), |acc, x| format!("{}{}", acc, x))
}

#[aoc(day16, part2)]
fn part2(input: &[isize]) -> String {
    let mut v: Vec<isize> = input
        .iter()
        .map(|&x| x as isize)
        .cycle()
        .take(10000 * input.len())
        .collect();
    let o = v[0..7]
        .into_iter()
        .fold(String::new(), |acc, x| format!("{}{}", acc, x))
        .parse()
        .unwrap();
    println!("{} - {} = {}", v.len(), o, v.len() - o);
    v = v[o..].to_vec();
    let bar = ProgressBar::new(100).with_style(ProgressStyle::default_bar().template(
        "{elapsed_precise} {wide_bar:.yellow/orange} {pos:>5}/{len:<5} ETA: {eta_precise}",
    ));
    bar.tick();
    for _ in 0..100 {
        v = (0..v.len())
            .into_par_iter()
            .map(|n| {
                let mut res = 0;
                for k in 0.. {
                    let a = (4 * k + 1) * (n + o + 1) - o;
                    if a > v.len() {
                        break;
                    }
                    for x in 0..=(n + o) {
                        let b = a + x;
                        if b > v.len() {
                            break;
                        }
                        res += v[b - 1];
                        let c = b + 2 * (n + o + 1);
                        if c <= v.len() {
                            res -= v[c - 1];
                        }
                    }
                }
                res.abs() % 10
            })
            .collect();
        bar.inc(1);
    }
    bar.finish();
    v[0..8]
        .into_iter()
        .fold(String::new(), |acc, x| format!("{}{}", acc, x))
}
