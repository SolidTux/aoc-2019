use crate::utils::{Computer, StepResult::*};
use aoc_runner_derive::aoc;
use std::{
    collections::BTreeMap,
    convert::{TryFrom, TryInto},
    fmt,
    io::Write,
    thread,
    time::Duration,
};
use Tile::*;

enum Tile {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball,
}

impl TryFrom<i128> for Tile {
    type Error = String;

    fn try_from(x: i128) -> Result<Self, Self::Error> {
        match x {
            0 => Ok(Empty),
            1 => Ok(Wall),
            2 => Ok(Block),
            3 => Ok(Paddle),
            4 => Ok(Ball),
            y => Err(format!("Unknown tile id {}.", y)),
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Empty => "  ",
            Wall => "██",
            Block => "🧱",
            Paddle => "🏓",
            Ball => "⚽",
        };
        write!(f, "{}", s)
    }
}

#[aoc(day13, part1)]
fn part1(input: &str) -> usize {
    let mut count = 0;
    let mut res = 0;
    let mut c = Computer::from_input(input);
    loop {
        match c.step() {
            Finished => break,
            Output(x) => {
                count = (count + 1) % 3;
                if count == 0 {
                    if x == 2 {
                        res += 1;
                    }
                }
            }
            _ => {}
        }
    }
    res
}

fn print(field: &BTreeMap<(i128, i128), Tile>, w: i128, h: i128, score: i128) {
    println!("{}[2JScore: {}", 27 as char, score);
    for y in 0..=h {
        for x in 0..=w {
            print!("{}", field.get(&(x, y)).unwrap_or(&Empty));
        }
        println!();
    }
    std::io::stdout().flush().unwrap();
    thread::sleep(Duration::from_millis(2));
}

#[aoc(day13, part2)]
fn part2(input: &str) -> i128 {
    let mut c = Computer::from_input(input);
    c.set_memory(0, 2);
    let mut field = BTreeMap::new();
    let mut w = 0;
    let mut h = 0;
    let mut score = 0;
    let mut paddle = 0;
    let mut ball = 0;
    let mut output = Vec::new();
    loop {
        match c.step() {
            Finished => break,
            Input => c.set_input((ball - paddle as i128).signum()),
            Output(x) => {
                output.push(x);
                if output.len() == 3 {
                    let x = output[0];
                    let y = output[1];
                    if x == -1 && y == 0 {
                        score = output[2];
                        print(&field, w, h, score);
                    } else {
                        w = w.max(x);
                        h = h.max(y);
                        let t: Tile = output[2].try_into().unwrap();
                        match t {
                            Ball => ball = x,
                            Paddle => paddle = x,
                            _ => {}
                        }
                        field.insert((x, y), t);
                    }
                    output.clear();
                }
            }
            Continue => {}
        }
    }
    score
}
