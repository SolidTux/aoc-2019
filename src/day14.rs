use aoc_runner_derive::{aoc, aoc_generator};
use std::{collections::HashMap, str::FromStr};
use Ingredient::*;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Ingredient {
    Ore,
    Fuel,
    Chemical(String),
}

impl FromStr for Ingredient {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "ORE" => Ore,
            "FUEL" => Fuel,
            s => Chemical(s.to_string()),
        })
    }
}

fn part(s: &str) -> Result<(Ingredient, usize), String> {
    let mut parts = s.split(" ");
    let a = parts
        .next()
        .ok_or(format!("Failed to get ingredient from {}.", s))?
        .parse()
        .map_err(|e| format!("{}", e))?;
    let b = parts
        .next()
        .ok_or(format!("Failed to get ingredient from {}.", s))?
        .parse()
        .map_err(|_| format!("Failed to get ingredient from {}.", s))?;
    Ok((b, a))
}

#[aoc_generator(day14)]
fn parse(input: &str) -> Result<Vec<((Ingredient, usize), Vec<(Ingredient, usize)>)>, String> {
    let mut res = Vec::new();
    for l in 
        //input
"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX"
            .lines() {
        let parts = l.split(" => ").collect::<Vec<_>>();
        let output = part(
            parts
                .get(1)
                .ok_or(format!("Failed to get reaction from string {}.", l))?,
        )?;
        let inputs = parts
            .get(0)
            .ok_or(format!("Failed to get reaction from string {}.", l))?
            .split(", ")
            .map(part)
            .collect::<Result<Vec<(Ingredient, usize)>, String>>()?;
        res.push((output, inputs));
    }
    Ok(res)
}

#[aoc(day14, part1)]
fn part1(input: &[((Ingredient, usize), Vec<(Ingredient, usize)>)]) -> usize {
    let direct: Vec<Ingredient> = input
        .iter()
        .cloned()
        .filter(|(_, v)| v.len() == 1 && v[0].0 == Ore)
        .map(|x| (x.0).0)
        .collect();
    let mut pool = input
        .iter()
        .cloned()
        .find(|(x, _)| x.0 == Fuel)
        .unwrap()
        .clone()
        .1
        .into_iter()
        .collect::<HashMap<Ingredient, usize>>();
    while !pool.keys().all(|x| direct.contains(x)) {
        let mut next = HashMap::new();
        for (k, v) in &pool {
            if direct.contains(&k) {
                *(next.entry(k.clone()).or_insert(0)) += v;
            } else {
                let ((_, n), vec) = input.iter().find(|&(x, _)| &x.0 == k).unwrap();
                println!("{} {}", v, n);
                for (a, m) in vec {
                    *(next.entry(a.clone()).or_insert(0)) +=
                        m * (*v as f64 / *n as f64).ceil() as usize;
                }
            }
        }
        pool = next;
    }
    let mut res = 0;
    for (k, v) in pool {
        let ((_, n), vec) = input.iter().find(|&(x, _)| x.0 == k).unwrap();
        let m = vec[0].1;
        res += m * (v as f64 / *n as f64).ceil() as usize;
    }
    res
}
