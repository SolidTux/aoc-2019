use aoc_runner_derive::{aoc, aoc_generator};
use std::{
    collections::{BTreeMap, BTreeSet},
    fmt,
};

use Entry::*;

#[aoc_generator(day6)]
fn orbits(input: &str) -> Vec<(String, String)> {
    input
        .lines()
        .map(|x| {
            let mut parts = x.trim().split(")");
            (
                parts.next().unwrap().to_string(),
                parts.next().unwrap().to_string(),
            )
        })
        .collect()
}

#[derive(PartialEq, Eq, Clone)]
enum Entry {
    Node(String),
    Num(usize),
}

impl Entry {
    pub fn is_num(&self) -> bool {
        match self {
            Node(_) => false,
            Num(_) => true,
        }
    }

    pub fn num(&self) -> Option<usize> {
        match self {
            Node(_) => None,
            Num(n) => Some(*n),
        }
    }
}

impl fmt::Debug for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Node(s) => write!(f, "{}", s),
            Num(s) => write!(f, "{}", s),
        }
    }
}

#[aoc(day6, part1)]
fn part1(input: &[(String, String)]) -> usize {
    let mut count = BTreeMap::new();
    let mut nodes = BTreeSet::new();
    for (b, a) in input {
        nodes.insert(a.to_string());
        nodes.insert(b.to_string());
        (*(count.entry(a.to_string()).or_insert(Vec::new()))).push(Node(b.to_string()));
    }
    for n in nodes {
        if !count.contains_key(&n) {
            count.insert(n, vec![Num(0)]);
        }
    }
    while !count.values().all(|y| y.iter().all(|x| x.is_num())) {
        let mut next = BTreeMap::new();
        for (k, v) in &count {
            if v.iter().all(|x| x.is_num()) {
                next.insert(k.to_string(), v.clone());
            } else {
                let mut v2 = Vec::new();
                for x in v {
                    match x {
                        Node(s) => {
                            let v3 = count.get(s).unwrap();
                            if v3.iter().all(|x| x.is_num()) {
                                let n: usize = v3.iter().filter_map(|x| x.num()).sum();
                                v2.push(Num(n + v3.len()));
                            } else {
                                v2.push(x.clone());
                            }
                        }
                        Num(_) => v2.push(x.clone()),
                    }
                }
                next.insert(k.to_string(), v2);
            }
        }
        count = next;
    }
    count
        .values()
        .map(|x| x.iter().filter_map(|x| x.num()).sum::<usize>())
        .sum()
}

#[aoc(day6, part2)]
fn part2(input: &[(String, String)]) -> usize {
    let mut edges = BTreeMap::new();
    for (a, b) in input {
        (*(edges.entry(a.to_string()).or_insert(Vec::new()))).push(b.to_string());
        (*(edges.entry(b.to_string()).or_insert(Vec::new()))).push(a.to_string());
    }
    let mut visited = BTreeSet::new();
    visited.insert("YOU".to_string());
    let mut current = visited.clone();
    for i in 1.. {
        let mut next = BTreeSet::new();
        for c in &current {
            for n in &edges[c] {
                if n == "SAN" {
                    return i - 2;
                } else if !visited.contains(n) {
                    next.insert(n.clone());
                }
            }
        }
        current = next;
    }
    unreachable!()
}
