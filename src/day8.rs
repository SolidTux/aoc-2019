use aoc_runner_derive::{aoc, aoc_generator};
use image::GrayImage;
use ndarray::prelude::*;

const W: usize = 25;
const H: usize = 6;

#[aoc_generator(day8)]
fn parse(input: &str) -> Array3<u32> {
    //let input = "123456789012";
    let digits: Vec<u32> = input.chars().filter_map(|x| x.to_digit(10)).collect();
    let layers = digits.len() / (W * H);
    Array3::from_shape_vec((layers, H, W), digits).unwrap()
}

#[aoc(day8, part1)]
fn part1(input: &Array3<u32>) -> usize {
    let mut res = 0;
    let mut m = std::usize::MAX;
    for i in 0..input.shape()[0] {
        let mut count = vec![0; 10];
        for &x in input.slice(s![i, .., ..]) {
            count[x as usize] += 1;
        }
        if count[0] < m {
            m = count[0];
            res = count[1] * count[2];
        }
    }
    res
}

#[aoc(day8, part2)]
fn part2(input: &Array3<u32>) -> usize {
    let mut res = Array2::zeros((H, W));
    let layers = input.shape()[0];
    for x in 0..W {
        for y in 0..H {
            for l in 0..layers {
                match input[[l, y, x]] {
                    2 => continue,
                    1 => res[[y, x]] = 255,
                    _ => {}
                }
                break;
            }
        }
    }
    GrayImage::from_raw(W as u32, H as u32, res.into_raw_vec())
        .unwrap()
        .save("day8.png")
        .unwrap();
    0
}
