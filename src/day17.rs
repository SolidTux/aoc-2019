use crate::utils::Computer;
use aoc_runner_derive::{aoc, aoc_generator};
use std::{collections::BTreeSet, io::Write, thread, time::Duration};

#[derive(Debug, Clone)]
struct Field {
    pub scaffolds: BTreeSet<(isize, isize)>,
    pub pos: (isize, isize),
    pub size: (isize, isize),
}

#[aoc_generator(day17, part1)]
fn parse(input: &str) -> Field {
    let mut c = Computer::from_input(input);
    let mut scaffolds = BTreeSet::new();
    let mut x = 0;
    let mut y = 0;
    let mut pos = (0, 0);
    while let Some(i) = c.output() {
        match i as u8 as char {
            '\n' => {
                x = 0;
                y += 1;
            }
            '#' => {
                scaffolds.insert((x, y));
                x += 1;
            }
            '>' | '<' | '^' | 'v' => {
                scaffolds.insert((x, y));
                pos = (x, y);
                x += 1;
            }
            '.' => x += 1,
            _ => unreachable!(),
        }
    }
    Field {
        scaffolds,
        pos,
        size: (x, y),
    }
}

#[aoc(day17, part1)]
fn part1(input: &Field) -> isize {
    let mut res = 0;
    for &(x0, y0) in &input.scaffolds {
        if [(x0 + 1, y0), (x0 - 1, y0), (x0, y0 + 1), (x0, y0 - 1)]
            .into_iter()
            .all(|x| input.scaffolds.contains(x))
        {
            res += x0 * y0;
        }
    }
    res
}

#[aoc(day17, part2)]
fn part2(input: &str) -> i128 {
    let mut c = Computer::from_input(input);
    while let Some(i) = c.output() {
        print!("{}", i as u8 as char);
    }
    c.reset();
    c.set_memory(0, 2);
    c.input_str("A,B,A,B,C,C,B,A,C,A\n");
    c.input_str("L,10,R,8,R,6,R,10\n");
    c.input_str("L,12,R,8,L,12\n");
    c.input_str("L,10,R,8,R,8\n");
    c.input_str("y\n");
    let mut last = 0;
    let mut s = String::new();
    while let Some(i) = c.output() {
        if i == 10 && last == 10 {
            println!("{}[2J{}", 27 as char, s);
            ::std::io::stdout().flush().unwrap();
            s.clear();
            thread::sleep(Duration::from_millis(10));
        } else {
            if i > 256 {
                return i;
            }
            s += &format!("{}", i as u8 as char);
        }
        last = i;
    }
    0
}
