use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day2)]
fn ints(input: &str) -> Vec<usize> {
    input
        .split(",")
        .map(|x| x.trim().parse().unwrap())
        .collect()
}

fn eval(input: &[usize], a: usize, b: usize) -> usize {
    let mut data = input.to_vec();
    data[1] = a;
    data[2] = b;
    let mut i = 0;
    while data[i] != 99 {
        match data[i] {
            1 => {
                let a = data[i + 1];
                let b = data[i + 2];
                let c = data[i + 3];
                data[c] = data[a] + data[b];
                i += 4;
            }
            2 => {
                let a = data[i + 1];
                let b = data[i + 2];
                let c = data[i + 3];
                data[c] = data[a] * data[b];
                i += 4;
            }
            _ => unreachable!(),
        }
    }
    data[0]
}

#[aoc(day2, part1)]
fn part1(input: &[usize]) -> usize {
    eval(input, 12, 2)
}

#[aoc(day2, part2)]
fn part2(input: &[usize]) -> usize {
    for a in 0..100 {
        for b in 0..100 {
            if eval(input, a, b) == 19690720 {
                return 100 * a + b;
            }
        }
    }
    unreachable!()
}
