use aoc_runner_derive::aoc_lib;
use Direction::*;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;

mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

mod utils;

#[derive(Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn update_pos(&self, pos: &mut (isize, isize)) {
        *pos = match self {
            Up => (pos.0, pos.1 + 1),
            Down => (pos.0, pos.1 - 1),
            Left => (pos.0 - 1, pos.1),
            Right => (pos.0 + 1, pos.1),
        };
    }
}

aoc_lib! { year = 2019 }
