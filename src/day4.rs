use crate::utils::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::BTreeSet;

#[aoc_generator(day4)]
fn generator(input: &str) -> (usize, usize) {
    let mut parts = input.trim().split("-");
    let a: usize = parts.next().unwrap().parse().unwrap();
    let b: usize = parts.next().unwrap().parse().unwrap();
    (a, b)
}

#[aoc(day4, part1)]
fn part1(&(a, b): &(usize, usize)) -> usize {
    (a..=b)
        .filter(|&x| {
            let d = digits(x, 10);
            let mut double = false;
            for i in 0..d.len() - 1 {
                if d[i] < d[i + 1] {
                    return false;
                }
                if d[i] == d[i + 1] {
                    double = true;
                }
            }
            double
        })
        .count()
}

#[aoc(day4, part2)]
fn part2(&(a, b): &(usize, usize)) -> usize {
    (a..=b)
        .filter(|&x| {
            let d = digits(x, 10);
            let mut groups = BTreeSet::new();
            let mut last = d[0];
            let mut counter = 1;
            for i in 1..d.len() {
                if d[i] > last {
                    return false;
                }
                if d[i] == last {
                    counter += 1;
                } else {
                    groups.insert(counter);
                    counter = 1;
                    last = d[i];
                }
            }
            groups.insert(counter);
            groups.iter().any(|&x| x == 2)
        })
        .count()
}
