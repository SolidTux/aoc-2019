use aoc_runner_derive::aoc;

#[aoc(day1, part1)]
fn part1(input: &str) -> usize {
    input
        .lines()
        .map(|x| {
            let n = x.parse::<usize>().unwrap();
            n / 3 - 2
        })
        .sum::<usize>()
}

#[aoc(day1, part2)]
fn part2(input: &str) -> isize {
    input
        .lines()
        .map(|x| {
            let n = x.parse::<isize>().unwrap();
            let mut m = n / 3 - 2;
            let mut res = 0;
            while m > 0 {
                res += m;
                m = m / 3 - 2;
            }
            res
        })
        .sum::<isize>()
}
