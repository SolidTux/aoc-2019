use crate::utils::Computer;
use aoc_runner_derive::aoc;
use std::io;

#[aoc(day25, part1)]
fn part1(input: &str) -> i128 {
    let stdin = io::stdin();
    let mut c = Computer::from_input(input);
    loop {
        match c.output_line() {
            Ok(l) => {
                print!("{}", l);
                if &l == "Command?\n" {
                    let mut s = String::new();
                    stdin.read_line(&mut s).unwrap();
                    c.input_str(&s);
                }
            }
            Err(i) => return i,
        }
    }
}
