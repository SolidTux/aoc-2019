use crate::utils::{Computer, StepResult::*};
use aoc_runner_derive::aoc;
use image::GrayImage;
use std::{
    collections::BTreeMap,
    convert::{TryFrom, TryInto},
};
use Color::*;
use Direction::*;

enum Color {
    Black,
    White,
}

impl TryFrom<i128> for Color {
    type Error = String;

    fn try_from(i: i128) -> Result<Self, Self::Error> {
        match i {
            0 => Ok(Black),
            1 => Ok(White),
            x => Err(format!("Unknown color index {}.", x)),
        }
    }
}

impl Into<u8> for &Color {
    fn into(self) -> u8 {
        match self {
            Black => 0,
            White => 255,
        }
    }
}

impl Into<i128> for &Color {
    fn into(self) -> i128 {
        match self {
            Black => 0,
            White => 1,
        }
    }
}

enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    pub fn turn(&mut self, i: i128) {
        *self = if i == 0 {
            match self {
                Left => Down,
                Right => Up,
                Up => Left,
                Down => Right,
            }
        } else {
            match self {
                Left => Up,
                Right => Down,
                Up => Right,
                Down => Left,
            }
        }
    }

    pub fn update(&self, pos: (isize, isize)) -> (isize, isize) {
        match self {
            Left => (pos.0 - 1, pos.1),
            Right => (pos.0 + 1, pos.1),
            Up => (pos.0, pos.1 + 1),
            Down => (pos.0, pos.1 - 1),
        }
    }
}

#[aoc(day11, part1)]
fn part1(input: &str) -> usize {
    let mut c = Computer::from_input(input);
    let mut fields: BTreeMap<(isize, isize), Color> = BTreeMap::new();
    let mut pos = (0, 0);
    let mut color = None;
    let mut dir = Up;
    loop {
        match c.step() {
            Finished => break,
            Output(x) => {
                if let Some(c) = color {
                    *(fields.entry(pos).or_insert(Black)) = c;
                    dir.turn(x);
                    pos = dir.update(pos);
                    color = None;
                } else {
                    color = Some(x.try_into().unwrap())
                }
            }
            Input => c.set_input(fields.get(&pos).unwrap_or(&Black).into()),
            Continue => {}
        }
    }
    fields.len()
}

#[aoc(day11, part2)]
fn part2(input: &str) -> &'static str {
    let mut c = Computer::from_input(input);
    let mut fields: BTreeMap<(isize, isize), Color> = BTreeMap::new();
    fields.insert((0, 0), White);
    let mut pos = (0, 0);
    let mut color = None;
    let mut dir = Up;
    let mut min = (0, 0);
    let mut max = (0, 0);
    loop {
        match c.step() {
            Finished => break,
            Output(x) => {
                if let Some(c) = color {
                    *(fields.entry(pos).or_insert(Black)) = c;
                    dir.turn(x);
                    pos = dir.update(pos);
                    min.0 = min.0.min(pos.0);
                    min.1 = min.1.min(pos.1);
                    max.0 = max.0.max(pos.0);
                    max.1 = max.1.max(pos.1);
                    color = None;
                } else {
                    color = Some(x.try_into().unwrap())
                }
            }
            Input => c.set_input(fields.get(&pos).unwrap_or(&Black).into()),
            Continue => {}
        }
    }
    let w = max.0 - min.0 + 1;
    let h = max.1 - min.1 + 1;
    let mut buffer = vec![0; (w * h) as usize];
    for x in min.0..=max.0 {
        for y in min.1..=max.1 {
            let ind = (x - min.0 + w * (h - y + min.1 - 1)) as usize;
            buffer[ind] = fields.get(&(x, y)).unwrap_or(&Black).into();
        }
    }
    let image = GrayImage::from_raw(w as u32, h as u32, buffer).unwrap();
    image.save("day11.png").unwrap();
    ""
}
