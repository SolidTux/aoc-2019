use crate::utils::Computer;
use aoc_runner_derive::aoc;
use image::{
    imageops::{self, FilterType},
    GrayImage,
};
use ndarray::prelude::*;
use std::collections::{BTreeMap, BTreeSet};
use Direction::*;

#[derive(Debug)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Into<i128> for &Direction {
    fn into(self) -> i128 {
        match self {
            Up => 1,
            Down => 2,
            Left => 3,
            Right => 4,
        }
    }
}

impl Direction {
    pub fn update(&self, pos: (i128, i128)) -> (i128, i128) {
        match self {
            Left => (pos.0 - 1, pos.1),
            Right => (pos.0 + 1, pos.1),
            Up => (pos.0, pos.1 + 1),
            Down => (pos.0, pos.1 - 1),
        }
    }
}

fn oxygen(input: &str) -> (usize, (i128, i128), Computer) {
    let mut current = BTreeMap::new();
    let mut visited = BTreeSet::new();
    current.insert((0, 0), Computer::from_input(input));
    visited.insert((0, 0));
    for n in 1.. {
        let mut next = BTreeMap::new();
        for (&pos, c) in &current {
            for dir in &[Up, Down, Left, Right] {
                let p = dir.update(pos);
                if visited.contains(&p) {
                    continue;
                }

                let mut c = c.clone();
                c.input(dir.into());
                let o = c.output().unwrap();
                match o {
                    0 => {}
                    1 => {
                        next.insert(p, c);
                        visited.insert(p.clone());
                    }
                    2 => return (n, p, c),
                    _ => unreachable!(),
                }
            }
        }
        current = next;
    }
    unreachable!()
}

#[aoc(day15, part1)]
fn part1(input: &str) -> usize {
    oxygen(input).0
}

#[aoc(day15, part1, image)]
fn image(input: &str) -> usize {
    let (_, p, c) = oxygen(input);
    let mut visited = BTreeSet::new();
    let mut current = BTreeMap::new();
    current.insert(p.clone(), c);
    visited.insert(p.clone());
    let mut xmin = 0;
    let mut xmax = 0;
    let mut ymin = 0;
    let mut ymax = 0;
    loop {
        let mut next = BTreeMap::new();
        for (&pos, c) in &current {
            for dir in &[Up, Down, Left, Right] {
                let p = dir.update(pos);
                if visited.contains(&p) {
                    continue;
                }
                xmin = xmin.min(p.0);
                xmax = xmax.max(p.0);
                ymin = ymin.min(p.1);
                ymax = ymax.max(p.1);

                let mut c = c.clone();
                c.input(dir.into());
                let o = c.output().unwrap();
                match o {
                    0 => {}
                    1 => {
                        next.insert(p, c);
                        visited.insert(p.clone());
                    }
                    _ => unreachable!(),
                }
            }
        }
        if next.is_empty() {
            break;
        }
        current = next;
    }
    let w = (xmax - xmin + 1) as usize;
    let h = (ymax - ymin + 1) as usize;
    let mut data: Array2<u8> = Array2::zeros((w, h));
    for x in xmin..=xmax {
        for y in ymin..=ymax {
            if visited.contains(&(x, y)) {
                data[[(x - xmin) as usize, (y - ymin) as usize]] = 255;
            }
        }
    }
    data[[(-xmin) as usize, (-ymin) as usize]] = 128;
    data[[(p.0 - xmin) as usize, (p.1 - ymin) as usize]] = 128;
    println!("{}", data);
    imageops::resize(
        &GrayImage::from_raw(w as u32, h as u32, data.into_raw_vec()).unwrap(),
        20 * w as u32,
        20 * h as u32,
        FilterType::Nearest,
    )
    .save("day15.png")
    .unwrap();
    0
}

#[aoc(day15, part2)]
fn part2(input: &str) -> usize {
    let (_, p, c) = oxygen(input);
    let mut visited = BTreeSet::new();
    let mut current = BTreeMap::new();
    current.insert(p.clone(), c);
    visited.insert(p);
    for n in 0.. {
        let mut next = BTreeMap::new();
        for (&pos, c) in &current {
            for dir in &[Up, Down, Left, Right] {
                let p = dir.update(pos);
                if visited.contains(&p) {
                    continue;
                }

                let mut c = c.clone();
                c.input(dir.into());
                let o = c.output().unwrap();
                match o {
                    0 => {}
                    1 => {
                        next.insert(p, c);
                        visited.insert(p.clone());
                    }
                    _ => unreachable!(),
                }
            }
        }
        if next.len() == 0 {
            return n;
        }
        current = next;
    }
    unreachable!()
}
