use aoc_runner_derive::{aoc, aoc_generator};
use ndarray::prelude::*;
use std::collections::BTreeSet;

fn ind(x: u32, y: u32) -> u32 {
    x + 5 * y
}

#[aoc_generator(day24)]
fn parse(input: &str) -> u32 {
    let mut i = 0;
    let mut res = 0;
    for (y, l) in input.lines().enumerate() {
        for (x, c) in l.trim().chars().enumerate() {
            if c == '#' {
                res |= 1 << ind(x as u32, y as u32);
            }
        }
    }
    res
}

fn test(m: u32, x: u32, y: u32) -> bool {
    m & (1 << ind(x, y)) != 0
}

fn set(m: &mut u32, x: u32, y: u32, b: bool) {
    *m |= 1 << ind(x, y);
    if !b {
        *m ^= 1 << ind(x, y);
    }
}

fn step(m: u32) -> u32 {
    let mut res = m;
    for x0 in 0..5_isize {
        for y0 in 0..5_isize {
            let mut count = 0;
            for &(x, y) in &[(x0 - 1, y0), (x0, y0 + 1), (x0 + 1, y0), (x0, y0 - 1)] {
                if x >= 0 && x < 5 && y >= 0 && y < 5 {
                    let x = x as u32;
                    let y = y as u32;
                    if test(m, x, y) {
                        count += 1;
                    }
                }
            }
            let x0 = x0 as u32;
            let y0 = y0 as u32;
            if test(m, x0, y0) {
                if count != 1 {
                    set(&mut res, x0, y0, false);
                }
            } else {
                if count == 1 || count == 2 {
                    set(&mut res, x0, y0, true);
                }
            }
        }
    }
    res
}

fn print(m: u32) {
    for y in 0..5 {
        for x in 0..5 {
            if test(m, x, y) {
                print!("🐛");
            } else {
                print!("🧱");
            }
        }
        println!();
    }
    println!();
}

#[aoc(day24, part1)]
fn part1(input: &u32) -> u32 {
    let mut steps = BTreeSet::new();
    let mut m = *input;
    steps.insert(m);
    print(m);
    loop {
        m = step(m);
        if steps.contains(&m) {
            return m;
        }
        print(m);
        steps.insert(m);
    }
}
