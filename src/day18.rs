use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{BTreeMap, BTreeSet};

use Field::*;

#[derive(Debug, Clone)]
enum Field {
    Empty,
    Key(char),
    Door(char),
}

#[derive(Debug, Default, Clone)]
struct Map {
    pub fields: BTreeMap<(usize, usize), Field>,
    pub keys: BTreeSet<char>,
    pub start: (usize, usize),
}

#[aoc_generator(day18)]
fn parse(input: &str) -> Map {
    let mut res = Map::default();
    for (y, l) in input.lines().enumerate() {
        for (x, c) in l.chars().enumerate() {
            match c {
                '.' => {
                    res.fields.insert((x, y), Empty);
                }
                '@' => {
                    res.fields.insert((x, y), Empty);
                    res.start = (x, y);
                }
                '#' => {}
                d => {
                    if d.is_lowercase() {
                        res.fields.insert((x, y), Key(d));
                        res.keys.insert(d);
                    } else {
                        let d = d.to_lowercase().next().unwrap();
                        res.fields.insert((x, y), Door(d));
                    }
                }
            }
        }
    }
    res
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct Position {
    pub pos: (usize, usize),
    pub keys: BTreeSet<char>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct Position2 {
    pub pos: [(usize, usize); 4],
    pub keys: BTreeSet<char>,
}

#[aoc(day18, part1)]
fn part1(input: &Map) -> usize {
    let mut current = BTreeSet::new();
    let mut visited: BTreeMap<BTreeSet<char>, BTreeSet<(usize, usize)>> = BTreeMap::new();
    visited.insert(
        BTreeSet::new(),
        [input.start].into_iter().copied().collect(),
    );
    current.insert(Position {
        pos: input.start,
        keys: BTreeSet::new(),
    });
    let mut steps = 1;
    while !current.is_empty() {
        let mut next = BTreeSet::new();
        for c in &current {
            for &(x, y) in &[
                (c.pos.0 - 1, c.pos.1),
                (c.pos.0 + 1, c.pos.1),
                (c.pos.0, c.pos.1 - 1),
                (c.pos.0, c.pos.1 + 1),
            ] {
                let mut c = c.clone();
                c.pos = (x, y);
                match input.fields.get(&(x, y)) {
                    Some(Empty) => {}
                    Some(Key(d)) => {
                        c.keys.insert(*d);
                    }
                    Some(Door(d)) => {
                        if !c.keys.contains(&d) {
                            continue;
                        }
                    }
                    None => continue,
                }
                if c.keys == input.keys {
                    return steps;
                }
                let v = visited.entry(c.keys.clone()).or_insert(BTreeSet::new());
                if v.contains(&(x, y)) {
                    continue;
                }
                v.insert((x, y));
                next.insert(c);
            }
        }
        current = next;
        steps += 1;
    }
    steps
}

#[aoc(day18, part2)]
fn part2(input: &Map) -> usize {
    let mut input = input.clone();
    let start = [
        (input.start.0 - 1, input.start.1 - 1),
        (input.start.0 - 1, input.start.1 + 1),
        (input.start.0 + 1, input.start.1 - 1),
        (input.start.0 + 1, input.start.1 + 1),
    ];
    input.fields.remove(&input.start);
    input.fields.remove(&(input.start.0 + 1, input.start.1));
    input.fields.remove(&(input.start.0 - 1, input.start.1));
    input.fields.remove(&(input.start.0, input.start.1 + 1));
    input.fields.remove(&(input.start.0, input.start.1 - 1));
    let mut current = BTreeSet::new();
    let mut visited: BTreeMap<BTreeSet<char>, Vec<BTreeSet<(usize, usize)>>> = BTreeMap::new();
    let startv = (0..4)
        .map(|i| {
            let mut res = BTreeSet::new();
            res.insert(start[i]);
            res
        })
        .collect();
    visited.insert(BTreeSet::new(), startv);
    current.insert(Position2 {
        pos: start,
        keys: BTreeSet::new(),
    });
    let mut steps = 1;
    while !current.is_empty() {
        let mut next = BTreeSet::new();
        for c in &current {
            for i in 0..4 {
                for &(x, y) in &[
                    (c.pos[i].0 - 1, c.pos[i].1),
                    (c.pos[i].0 + 1, c.pos[i].1),
                    (c.pos[i].0, c.pos[i].1 - 1),
                    (c.pos[i].0, c.pos[i].1 + 1),
                ] {
                    let mut c = c.clone();
                    c.pos[i] = (x, y);
                    match input.fields.get(&(x, y)) {
                        Some(Empty) => {}
                        Some(Key(d)) => {
                            c.keys.insert(*d);
                        }
                        Some(Door(d)) => {
                            if !c.keys.contains(&d) {
                                continue;
                            }
                        }
                        None => continue,
                    }
                    if c.keys == input.keys {
                        return steps;
                    }
                    let v = visited
                        .entry(c.keys.clone())
                        .or_insert((0..4).map(|_| BTreeSet::new()).collect());
                    if v[i].contains(&c.pos[i]) {
                        continue;
                    }
                    v[i].insert(c.pos[i].clone());
                    next.insert(c);
                }
            }
        }
        current = next;
        steps += 1;
    }
    steps
}
