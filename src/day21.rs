use crate::utils::Computer;
use aoc_runner_derive::aoc;

#[aoc(day21, part1)]
fn part1(input: &str) -> i128 {
    let mut c = Computer::from_input(input);
    c.output_line().unwrap();
    c.input_str(
        "NOT A J
NOT B T
OR T J
NOT C T
OR T J
NOT D T
NOT T T
AND T J
WALK
",
    );
    loop {
        match c.output_line() {
            Ok(l) => {
                if l.len() == 0 {
                    return 0;
                } else {
                    print!("{}", l);
                }
            }
            Err(i) => return i,
        }
    }
}

#[aoc(day21, part2)]
fn part2(input: &str) -> i128 {
    let mut c = Computer::from_input(input);
    c.output_line().unwrap();
    c.input_str(
        "NOT A J
NOT B T
OR T J
NOT C T
OR T J
NOT D T
NOT T T
AND T J
NOT E T
NOT T T
OR H T
AND T J
RUN
",
    );
    loop {
        match c.output_line() {
            Ok(l) => {
                if l.len() == 0 {
                    return 0;
                } else {
                    print!("{}", l);
                }
            }
            Err(i) => return i,
        }
    }
}
