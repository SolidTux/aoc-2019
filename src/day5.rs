use crate::utils::digits;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day5)]
fn ints(input: &str) -> Vec<isize> {
    input
        .split(",")
        .map(|x| x.trim().parse().unwrap())
        .collect()
}

fn eval(code: &[isize], input: &[isize]) -> Vec<isize> {
    let mut data = code.to_vec();
    let mut i = 0;
    let mut inp = 0;
    let mut output = Vec::new();
    while data[i] != 99 {
        let code = data[i] % 100;
        let d = digits(data[i] as usize / 100, 10);
        match code {
            1 | 2 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = data[i + 1];
                        data[x as usize]
                    }
                    1 => data[i + 1],
                    _ => unreachable!(),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = data[i + 2];
                        data[x as usize]
                    }
                    1 => data[i + 2],
                    _ => unreachable!(),
                };
                let c = data[i + 3];
                if code == 1 {
                    data[c as usize] = a + b;
                } else {
                    data[c as usize] = a * b;
                }
                i += 4;
            }
            3 => {
                let a = data[i + 1];
                data[a as usize] = input[inp];
                inp += 1;
                i += 2;
            }
            4 => {
                let ma = d.get(0).unwrap_or(&0);
                match ma {
                    0 => {
                        let x = data[i + 1];
                        output.push(data[x as usize]);
                    }
                    1 => output.push(data[i + 1]),
                    _ => unreachable!(),
                };
                i += 2;
            }
            5 | 6 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = data[i + 1];
                        data[x as usize]
                    }
                    1 => data[i + 1],
                    _ => unreachable!(),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = data[i + 2];
                        data[x as usize]
                    }
                    1 => data[i + 2],
                    _ => unreachable!(),
                };
                let cond = match code {
                    5 => a != 0,
                    6 => a == 0,
                    _ => unreachable!(),
                };
                if cond {
                    i = b as usize;
                } else {
                    i += 3;
                }
            }
            7 | 8 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = data[i + 1];
                        data[x as usize]
                    }
                    1 => data[i + 1],
                    _ => unreachable!(),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = data[i + 2];
                        data[x as usize]
                    }
                    1 => data[i + 2],
                    _ => unreachable!(),
                };
                let c = data[i + 3];
                let cond = match code {
                    7 => a < b,
                    8 => a == b,
                    _ => unreachable!(),
                };
                data[c as usize] = if cond { 1 } else { 0 };
                i += 4;
            }
            _ => unreachable!(),
        }
    }
    output
}

#[aoc(day5, part1)]
fn part1(input: &[isize]) -> isize {
    let res = eval(input, &[1]);
    res[res.len() - 1]
}

#[aoc(day5, part2)]
fn part2(input: &[isize]) -> isize {
    let res = eval(input, &[5]);
    res[res.len() - 1]
}
