use super::Direction::{self, *};
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, HashSet};

#[aoc_generator(day3)]
fn steps(input: &str) -> Vec<Vec<(Direction, usize)>> {
    input
        .lines()
        .map(|x| {
            x.trim()
                .split(",")
                .map(|y| {
                    let mut c = y.chars();
                    let t = c.next().unwrap();
                    let n = c.collect::<String>().parse().unwrap();
                    (
                        match t {
                            'U' => Up,
                            'D' => Down,
                            'L' => Left,
                            'R' => Right,
                            _ => unreachable!(),
                        },
                        n,
                    )
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

#[aoc(day3, part1)]
fn part1(input: &[Vec<(Direction, usize)>]) -> isize {
    let mut pos = HashSet::new();

    let mut current = (0, 0);
    for (dir, n) in &input[0] {
        for _ in 0..*n {
            dir.update_pos(&mut current);
            pos.insert(current);
        }
    }

    current = (0, 0);
    let mut intersections = HashSet::new();
    for (dir, n) in &input[1] {
        for _ in 0..*n {
            dir.update_pos(&mut current);
            if pos.contains(&current) {
                intersections.insert(current);
            }
        }
    }
    intersections
        .iter()
        .map(|x| x.0.abs() + x.1.abs())
        .min()
        .unwrap()
}

#[aoc(day3, part2)]
fn part2(input: &[Vec<(Direction, usize)>]) -> isize {
    let mut pos = HashMap::new();

    let mut current = (0, 0);
    let mut steps: isize = 0;
    for (dir, n) in &input[0] {
        for _ in 0..*n {
            steps += 1;
            dir.update_pos(&mut current);
            if !pos.contains_key(&current) {
                pos.insert(current, steps);
            }
        }
    }

    current = (0, 0);
    steps = 0;
    let mut intersections: HashMap<_, isize> = HashMap::new();
    for (dir, n) in &input[1] {
        for _ in 0..*n {
            steps += 1;
            dir.update_pos(&mut current);
            if let Some(m) = pos.get(&current) {
                if !intersections.contains_key(&current) {
                    intersections.insert(current, steps + m);
                }
            }
        }
    }
    intersections.values().min().unwrap().clone()
}
