use crate::utils::Computer;
use aoc_runner_derive::aoc;
use image::GrayImage;
use ndarray::prelude::*;
use std::collections::{BTreeMap, BTreeSet};

#[aoc(day19, part1)]
fn part1(input: &str) -> i128 {
    let mut res = 0;
    let mut c = Computer::from_input(input);
    for y in 0..50 {
        for x in 0..50 {
            c.input(x);
            c.input(y);
            let o = c.output().unwrap();
            c.reset();
            res += o;
            match o {
                0 => print!("."),
                1 => print!("#"),
                _ => unreachable!(),
            }
        }
        println!();
    }
    res
}

#[aoc(day19, part1, image)]
fn image(input: &str) -> String {
    let n = 500;
    let mut data = Array2::zeros((n, n));
    let mut c = Computer::from_input(input);
    for y in 0..n {
        for x in 0..n {
            c.input(x as i128);
            c.input(y as i128);
            data[[x, y]] = 255 * (c.output().unwrap() as u8);
            c.reset();
        }
    }
    GrayImage::from_raw(n as u32, n as u32, data.into_raw_vec())
        .unwrap()
        .save("day19.png")
        .unwrap();
    String::new()
}

const S: usize = 100;

#[aoc(day19, part2)]
fn part2(input: &str) -> usize {
    let mut current = BTreeSet::new();
    current.insert((0, 0));
    let mut visited = BTreeSet::new();
    let mut c = Computer::from_input(input);
    let mut cache = BTreeMap::new();
    let mut get = move |x, y| match cache.get(&(x, y)) {
        Some(res) => *res,
        None => {
            c.input(x as i128);
            c.input(y as i128);
            let res = c.output().unwrap();
            c.reset();
            cache.insert((x, y), res);
            res
        }
    };
    let mut max_size = 0;
    let mut max_pos = (0, 0);
    let res;
    'a: loop {
        println!("{:>4} {:>5}", current.len(), visited.len());
        let mut next = BTreeSet::new();
        let mut last = (0, 0);
        for p in current {
            //println!("{:?}", p);
            let mut size = 0;
            'nloop: for n in 1.. {
                for o in 0..=n {
                    for &(x, y) in &[(p.0 + o, p.1 + n), (p.0 + n, p.1 + o)] {
                        //println!("\t{:?} {}", (x, y), get(x, y));
                        if get(x, y) == 1 {
                            if !visited.contains(&(x, y)) {
                                next.insert((x, y));
                            }
                        } else {
                            size = n - 1;
                            break 'nloop;
                        }
                    }
                }
            }
            //println!("\tsize: {}", size);
            if size == S - 1 {
                res = p;
                break 'a;
            }
            max_size = max_size.max(size);
            max_pos.0 = max_pos.0.max(p.0);
            max_pos.1 = max_pos.1.max(p.1);
            visited.insert(p);
            last = p;
        }
        println!("\tsize: {:>3} pos: {:?}", max_size, max_pos);
        if next.is_empty() {
            let l = last.0.min(last.1);
            'outer: for n in 1.. {
                for o in 0..=n {
                    for &(x, y) in &[(l + o, l + n), (l + n, l + o)] {
                        if !visited.contains(&(x, y)) {
                            if get(x, y) == 1 {
                                next.insert((x, y));
                                break 'outer;
                            }
                        }
                    }
                }
            }
        }
        current = next;
    }

    //print!("  ");
    //for x in 0..=res.0 + S + 10 {
    //print!("{}", x / 10);
    //}
    //println!();
    //print!("  ");
    //for x in 0..=res.0 + S + 10 {
    //print!("{}", x % 10);
    //}
    //println!();
    //for y in 0..=res.1 + S + 10 {
    //print!("{:02}", y);
    //for x in 0..=res.0 + S + 10 {
    //match get(x, y) {
    //0 => print!("."),
    //1 => {
    //if (x, y) == res {
    //print!("*");
    //} else if (x, y) == (res.0 + S - 1, res.1 + S - 1) {
    //print!("*");
    //} else {
    //print!("#");
    //}
    //}
    //_ => unreachable!(),
    //}
    //}
    //println!();
    //}

    res.0 * 10000 + res.1
}
