#![allow(dead_code)]

use std::collections::BTreeMap;
use StepResult::*;

#[derive(Clone, Debug)]
pub struct Computer {
    data: BTreeMap<i128, i128>,
    start_data: BTreeMap<i128, i128>,
    input: Option<i128>,
    pntr: i128,
    base: i128,
}

#[derive(Debug, PartialEq, Eq)]
pub enum StepResult {
    Finished,
    Input,
    Output(i128),
    Continue,
}

impl Computer {
    pub fn from_input(input: &str) -> Self {
        let data = input
            .split(",")
            .map(|x| x.trim().parse().unwrap())
            .enumerate()
            .map(|(a, b)| (a as i128, b))
            .collect::<BTreeMap<i128, i128>>();
        Computer {
            start_data: data.clone(),
            data,
            input: None,
            pntr: 0,
            base: 0,
        }
    }

    pub fn set_memory(&mut self, i: i128, data: i128) {
        self.data.insert(i, data);
    }

    pub fn from_code(code: &[i128]) -> Self {
        let data = code
            .iter()
            .copied()
            .enumerate()
            .map(|(a, b)| (a as i128, b))
            .collect::<BTreeMap<i128, i128>>();
        Computer {
            start_data: data.clone(),
            data,
            input: None,
            pntr: 0,
            base: 0,
        }
    }

    pub fn run<'a, I>(&mut self, input: I) -> Vec<i128>
    where
        I: IntoIterator<Item = &'a i128>,
    {
        let mut output = Vec::new();
        let mut iter = input.into_iter();
        loop {
            match self.step() {
                Finished => break,
                Input => {
                    self.input(*iter.next().unwrap());
                }
                Output(x) => output.push(x),
                Continue => {}
            }
        }
        output
    }

    pub fn input(&mut self, input: i128) -> bool {
        loop {
            match self.step() {
                Input => break,
                Finished => return false,
                _ => {}
            }
        }
        self.input = Some(input);
        true
    }

    pub fn input_str(&mut self, input: &str) -> bool {
        for c in input.chars() {
            if !self.input(c as i128) {
                return false;
            }
        }
        true
    }

    pub fn set_input(&mut self, input: i128) {
        self.input = Some(input);
    }

    pub fn output(&mut self) -> Option<i128> {
        loop {
            match self.step() {
                Output(x) => return Some(x),
                Finished => return None,
                _ => {}
            }
        }
    }

    pub fn output_line(&mut self) -> Result<String, i128> {
        let mut res = String::new();
        while let Some(i) = self.output() {
            if i < 128 {
                let c = i as u8 as char;
                res += &format!("{}", c);
                if c == '\n' {
                    return Ok(res);
                }
            } else {
                return Err(i);
            }
        }
        Ok(res)
    }

    pub fn reset(&mut self) {
        self.pntr = 0;
        self.base = 0;
        self.data = self.start_data.clone();
    }

    pub fn step(&mut self) -> StepResult {
        let current = *self.data.get(&self.pntr).unwrap_or(&0);
        let code = current % 100;
        let d = digits(current as usize / 100, 10);
        let mut output = None;
        match code {
            1 | 2 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 1)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 2)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let mc = d.get(2).unwrap_or(&0);
                let c = match mc {
                    0 => self.data.get(&(self.pntr + 3)).unwrap_or(&0).clone(),
                    2 => self.data.get(&(self.pntr + 3)).unwrap_or(&0) + self.base,
                    _ => panic!("Error while handling code {}.", code),
                };
                if code == 1 {
                    *self.data.entry(c).or_insert(0) = a + b;
                } else {
                    *self.data.entry(c).or_insert(0) = a * b;
                }
                self.pntr += 4;
            }
            3 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => self.data.get(&(self.pntr + 1)).unwrap_or(&0).clone(),
                    2 => self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base,
                    _ => panic!("Error while handling code {}.", code),
                };
                if let Some(i) = self.input {
                    *self.data.entry(a).or_insert(0) = i;
                    self.input = None;
                    self.pntr += 2;
                } else {
                    return Input;
                }
            }
            4 => {
                let ma = d.get(0).unwrap_or(&0);
                match ma {
                    0 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0);
                        output = Some(*self.data.get(x).unwrap_or(&0));
                    }
                    1 => output = Some(*self.data.get(&(self.pntr + 1)).unwrap_or(&0)),
                    2 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base;
                        output = Some(*self.data.get(&x).unwrap_or(&0));
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                self.pntr += 2;
            }
            5 | 6 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 1)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 2)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let cond = match code {
                    5 => *a != 0,
                    6 => *a == 0,
                    _ => panic!("Error while handling code {}.", code),
                };
                if cond {
                    self.pntr = *b;
                } else {
                    self.pntr += 3;
                }
            }
            7 | 8 => {
                let ma = d.get(0).unwrap_or(&0);
                let a = match ma {
                    0 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 1)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let mb = d.get(1).unwrap_or(&0);
                let b = match mb {
                    0 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0);
                        self.data.get(x).unwrap_or(&0)
                    }
                    1 => self.data.get(&(self.pntr + 2)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 2)).unwrap_or(&0) + self.base;
                        self.data.get(&x).unwrap_or(&0)
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                let mc = d.get(2).unwrap_or(&0);
                let c = match mc {
                    0 => self.data.get(&(self.pntr + 3)).unwrap_or(&0).clone(),
                    2 => self.data.get(&(self.pntr + 3)).unwrap_or(&0) + self.base,
                    _ => panic!("Error while handling code {}.", code),
                };
                let cond = match code {
                    7 => a < b,
                    8 => a == b,
                    _ => panic!("Error while handling code {}.", code),
                };
                *self.data.entry(c).or_insert(0) = if cond { 1 } else { 0 };
                self.pntr += 4;
            }
            9 => {
                let ma = d.get(0).unwrap_or(&0);
                match ma {
                    0 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0);
                        self.base += self.data.get(x).unwrap_or(&0);
                    }
                    1 => self.base += self.data.get(&(self.pntr + 1)).unwrap_or(&0),
                    2 => {
                        let x = self.data.get(&(self.pntr + 1)).unwrap_or(&0) + self.base;
                        self.base += self.data.get(&x).unwrap_or(&0);
                    }
                    _ => panic!("Error while handling code {}.", code),
                };
                self.pntr += 2;
            }
            99 => return Finished,
            s => panic!("Unknown code {}.", s),
        }
        if let Some(o) = output {
            Output(o)
        } else {
            Continue
        }
    }
}

pub struct Polygonal {
    s: usize,
    n: usize,
}

impl Polygonal {
    pub fn new(s: usize) -> Polygonal {
        Polygonal { s, n: 0 }
    }
}

impl Iterator for Polygonal {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.n += 1;
        Some((self.n.pow(2) * (self.s - 2) - self.n * (self.s - 4)) / 2)
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.n = n + 1;
        Some((self.n.pow(2) * (self.s - 2) - self.n * (self.s - 4)) / 2)
    }
}

pub fn num_digits(n: usize, base: usize) -> usize {
    let mut res = 1;
    let mut x = n;
    while x > base - 1 {
        x /= base;
        res += 1;
    }
    res
}

pub fn cycle_digits(n: usize, base: usize) -> Vec<usize> {
    let d = num_digits(n, base);
    let mut res = vec![n];
    let mut x = n;
    for _ in 1..d {
        let y = x % base;
        x /= base;
        x += base.pow(d as u32 - 1) * y;
        res.push(x);
    }
    res
}

pub fn digits(n: usize, base: usize) -> Vec<usize> {
    let mut res = Vec::new();
    let mut x = n;
    for _ in 0..num_digits(n, base) {
        res.push(x % base);
        x /= base;
    }
    res
}

pub fn digits_to_num(digits: &[usize], base: usize) -> usize {
    digits
        .iter()
        .enumerate()
        .map(|(i, x)| x * base.pow(i as u32))
        .sum()
}
