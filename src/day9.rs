use crate::utils::Computer;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day9)]
fn ints(input: &str) -> Vec<i128> {
    input
        .split(",")
        .map(|x| x.trim().parse().unwrap())
        .collect()
}

#[aoc(day9, part1)]
fn part1(input: &[i128]) -> i128 {
    let res = Computer::from_code(input).run(&[1]);
    res[res.len() - 1]
}

#[aoc(day9, part2)]
fn part2(input: &[i128]) -> i128 {
    let res = Computer::from_code(input).run(&[2]);
    res[res.len() - 1]
}
