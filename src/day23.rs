use crate::utils::{Computer, StepResult::*};
use aoc_runner_derive::aoc;
use std::{
    sync::{
        mpsc::{self, TryRecvError},
        Arc, Mutex,
    },
    thread,
    time::Duration,
};

#[aoc(day23, part1)]
fn part1(input: &str) -> i128 {
    let c = Computer::from_input(input);
    let mut senders = Vec::new();
    let mut receivers = Vec::new();
    for _ in 0..50 {
        let (tx, rx) = mpsc::channel();
        senders.push(tx);
        receivers.push(rx);
    }
    let mut handles = Vec::new();
    let (tx, rx) = mpsc::channel();
    for i in 0..50 {
        let mut c = c.clone();
        let senders = senders.clone();
        let tx = tx.clone();
        let rx = receivers.pop().unwrap();
        handles.push(thread::spawn(move || {
            c.input(49 - i);
            let mut y_value = None;
            let mut output = Vec::new();
            loop {
                match c.step() {
                    Finished => break,
                    Input => {
                        if let Some(y) = y_value {
                            c.set_input(y);
                            y_value = None;
                        } else {
                            match rx.try_recv() {
                                Ok(Some((x, y))) => {
                                    c.set_input(x);
                                    y_value = Some(y);
                                }
                                Ok(None) => break,
                                Err(TryRecvError::Empty) => {
                                    c.set_input(-1);
                                }
                                Err(TryRecvError::Disconnected) => panic!("Disconnected channel."),
                            }
                        }
                    }
                    Output(x) => {
                        output.push(x);
                        if output.len() == 3 {
                            let id = output[0];
                            let pos = (output[1], output[2]);
                            if id == 255 {
                                tx.send(pos).unwrap();
                            } else {
                                senders[id as usize].send(Some(pos)).unwrap();
                            }
                            output.clear();
                        }
                    }
                    Continue => {}
                }
            }
        }));
    }
    let res = rx.recv().unwrap().1;
    for s in senders {
        let _ = s.send(None);
    }
    for h in handles {
        h.join().unwrap();
    }
    res
}

#[aoc(day23, part2)]
fn part2(input: &str) -> i128 {
    let c = Computer::from_input(input);
    let mut res = -1;
    while res == -1 {
        let mut senders = Vec::new();
        let mut receivers = Vec::new();
        let empty = Arc::new(Mutex::new(vec![0; 50]));
        for _ in 0..50 {
            let (tx, rx) = mpsc::channel();
            senders.push(tx);
            receivers.push(rx);
        }
        let mut handles = Vec::new();
        let (tx, rx) = mpsc::channel();
        for i in 0..50 {
            let mut c = c.clone();
            let senders = senders.clone();
            let tx = tx.clone();
            let rx = receivers.pop().unwrap();
            let empty = empty.clone();
            handles.push(thread::spawn(move || {
                c.input(49 - i as i128);
                let mut y_value = None;
                let mut output = Vec::new();
                loop {
                    match c.step() {
                        Finished => break,
                        Input => {
                            if let Some(y) = y_value {
                                c.set_input(y);
                                y_value = None;
                            } else {
                                match rx.try_recv() {
                                    Ok(Some((x, y))) => {
                                        c.set_input(x);
                                        y_value = Some(y);
                                    }
                                    Ok(None) => break,
                                    Err(TryRecvError::Empty) => {
                                        {
                                            let mut e = empty.lock().unwrap();
                                            e[i] += 1;
                                        }
                                        c.set_input(-1);
                                        thread::sleep(Duration::from_millis(10));
                                    }
                                    Err(TryRecvError::Disconnected) => {
                                        panic!("Disconnected channel.")
                                    }
                                }
                            }
                        }
                        Output(x) => {
                            output.push(x);
                            if output.len() == 3 {
                                let id = output[0] as usize;
                                let pos = (output[1], output[2]);
                                {
                                    let mut e = empty.lock().unwrap();
                                    e[i] = 0;
                                }
                                if id == 255 {
                                    tx.send(pos).unwrap();
                                } else {
                                    {
                                        let mut e = empty.lock().unwrap();
                                        e[id] = 0;
                                    }
                                    senders[id].send(Some(pos)).unwrap();
                                }
                                output.clear();
                            }
                        }
                        Continue => {}
                    }
                }
            }));
        }
        let mut memory = (0, 0);
        let mut last = None;
        loop {
            while let Ok(pos) = rx.try_recv() {
                memory = pos;
            }
            {
                let mut e = empty.lock().unwrap();
                let m = *e.iter().min().unwrap();
                if m > 2 {
                    if let Some(l) = last {
                        if memory.1 == l {
                            res = l;
                            break;
                        }
                    }
                    senders[0].send(Some(memory)).unwrap();
                    for i in 0..50 {
                        e[i] = 0;
                    }
                    last = Some(memory.1);
                }
            }
            thread::sleep(Duration::from_millis(10));
        }
        for s in senders {
            let _ = s.send(None);
        }
        for h in handles {
            h.join().unwrap();
        }
    }
    res
}
