use aoc_runner_derive::{aoc, aoc_generator};
use num_rational::Rational;
use std::{
    collections::{BTreeMap, BTreeSet},
    f64::consts::PI,
};

#[aoc_generator(day10)]
fn parse(input: &str) -> Vec<(isize, isize)> {
    let mut res = Vec::new();
    for (i, l) in input.lines().enumerate() {
        for (j, c) in l.trim().chars().enumerate() {
            if c == '#' {
                res.push((j as isize, i as isize))
            }
        }
    }
    res
}

fn pos(input: &[(isize, isize)]) -> ((isize, isize), usize) {
    let mut res = ((0, 0), 0);
    for &(x0, y0) in input {
        let mut v = BTreeSet::new();
        for &(x1, y1) in input {
            if x1 == x0 && y1 == y0 {
                continue;
            }
            let (x, y) = if y0 == y1 {
                if x1 > x0 {
                    (1, 0)
                } else {
                    (-1, 0)
                }
            } else if x0 == x1 {
                if y1 > y0 {
                    (0, 1)
                } else {
                    (0, -1)
                }
            } else {
                let r = Rational::new(x1 - x0, y1 - y0);
                let n = r.numer().abs();
                let d = r.denom().abs();
                if x1 > x0 {
                    if y1 > y0 {
                        (n, d)
                    } else {
                        (n, -d)
                    }
                } else {
                    if y1 > y0 {
                        (-n, d)
                    } else {
                        (-n, -d)
                    }
                }
            };
            v.insert((x, y));
        }
        if v.len() > res.1 {
            res.0 = (x0, y0);
            res.1 = v.len();
        }
    }
    res
}

#[aoc(day10, part1)]
fn part1(input: &[(isize, isize)]) -> usize {
    pos(input).1
}

fn dir(pos: &(isize, isize)) -> (usize, usize) {
    let phi = (pos.0 as f64).atan2(-pos.1 as f64) / PI;
    (
        (((phi + 2.) % 2.) * 1e8) as usize,
        (pos.0.pow(2) + pos.1.pow(2)) as usize,
    )
}

#[aoc(day10, part2)]
fn part2(input: &[(isize, isize)]) -> isize {
    let p = pos(input).0;
    let mut asteroids = input
        .iter()
        .map(|(a, b)| (a - p.0, b - p.1))
        .filter(|&(a, b)| !(a == 0 && b == 0))
        .collect::<Vec<_>>();
    let mut d = asteroids
        .iter()
        .cloned()
        .map(|x| (dir(&x), x))
        .collect::<BTreeMap<_, _>>();
    let angles = d
        .keys()
        .map(|x| x.0)
        .collect::<BTreeSet<_>>()
        .into_iter()
        .collect::<Vec<_>>();
    let mut c = 0;
    while asteroids.len() > 1 {
        for &a in &angles {
            if let Some(&(_, dist)) = d.keys().filter(|x| x.0 == a).min() {
                let ast = d.get(&(a, dist)).unwrap().clone();
                asteroids = asteroids.into_iter().filter(|&x| x != ast).collect();
                d = d.into_iter().filter(|&(_k, v)| v != ast).collect();
                if c == 199 {
                    return (ast.0 + p.0) * 100 + ast.1 + p.1;
                }
                c += 1;
            }
        }
    }
    unreachable!()
}
