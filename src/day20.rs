use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{BTreeMap, BTreeSet};

#[derive(Debug, Clone)]
struct Maze {
    pub fields: BTreeMap<(usize, usize), Option<((usize, usize), isize)>>,
    pub start: (usize, usize),
    pub end: (usize, usize),
    pub size: (usize, usize),
}

#[aoc_generator(day20)]
fn parse(input: &str) -> Maze {
    let mut fields = BTreeMap::new();
    let mut first_open = (0, 0);
    let mut last_open = (0, 0);
    let chars: Vec<Vec<char>> = input.lines().map(|x| x.chars().collect()).collect();
    let w = chars[0].len();
    let h = chars.len();
    for y in 2..h - 2 {
        for x in 2..w - 2 {
            match chars[y][x] {
                '.' => {
                    fields.insert((x, y), None);
                }
                ' ' => {
                    if first_open == (0, 0) {
                        first_open = (x, y);
                    }
                    last_open = (x, y);
                }
                _ => {}
            }
        }
    }
    let mut portals: BTreeMap<String, ((usize, usize), isize)> = BTreeMap::new();
    for x in 2..w - 2 {
        match chars[1][x] {
            ' ' => {}
            c => {
                let name = format!("{}{}", chars[0][x], c);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((x, 1), Some((p.0, 1)));
                    fields.insert(p.0, Some(((x, 1), p.1)));
                } else {
                    portals.insert(name, ((x, 1), 1));
                }
            }
        }
        match chars[h - 2][x] {
            ' ' => {}
            c => {
                let name = format!("{}{}", c, chars[h - 1][x]);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((x, h - 2), Some((p.0, 1)));
                    fields.insert(p.0, Some(((x, h - 2), p.1)));
                } else {
                    portals.insert(name, ((x, h - 2), 1));
                }
            }
        }
    }
    for x in first_open.0..=last_open.0 {
        match chars[last_open.1][x] {
            ' ' => {}
            c => {
                let name = format!("{}{}", chars[last_open.1 - 1][x], c);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((x, last_open.1), Some((p.0, -1)));
                    fields.insert(p.0, Some(((x, last_open.1), p.1)));
                } else {
                    portals.insert(name, ((x, last_open.1), -1));
                }
            }
        }
        match chars[first_open.1][x] {
            ' ' => {}
            c => {
                let name = format!("{}{}", c, chars[first_open.1 + 1][x]);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((x, first_open.1), Some((p.0, -1)));
                    fields.insert(p.0, Some(((x, first_open.1), p.1)));
                } else {
                    portals.insert(name, ((x, first_open.1), -1));
                }
            }
        }
    }
    for y in 2..h - 2 {
        match chars[y][1] {
            ' ' => {}
            c => {
                let name = format!("{}{}", chars[y][0], c);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((1, y), Some((p.0, 1)));
                    fields.insert(p.0, Some(((1, y), p.1)));
                } else {
                    portals.insert(name, ((1, y), 1));
                }
            }
        }
        match chars[y][w - 2] {
            ' ' => {}
            c => {
                let name = format!("{}{}", c, chars[y][w - 1]);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((w - 2, y), Some((p.0, 1)));
                    fields.insert(p.0, Some(((w - 2, y), p.1)));
                } else {
                    portals.insert(name, ((w - 2, y), 1));
                }
            }
        }
    }
    for y in first_open.1..=last_open.1 {
        match chars[y][last_open.0] {
            ' ' => {}
            c => {
                let name = format!("{}{}", chars[y][last_open.0 - 1], c);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((last_open.0, y), Some((p.0, -1)));
                    fields.insert(p.0, Some(((last_open.0, y), p.1)));
                } else {
                    portals.insert(name, ((last_open.0, y), -1));
                }
            }
        }
        match chars[y][first_open.0] {
            ' ' => {}
            c => {
                let name = format!("{}{}", c, chars[y][first_open.0 + 1]);
                if let Some(p) = portals.remove(&name) {
                    fields.insert((first_open.0, y), Some((p.0, -1)));
                    fields.insert(p.0, Some(((first_open.0, y), p.1)));
                } else {
                    portals.insert(name, ((first_open.0, y), -1));
                }
            }
        }
    }
    Maze {
        fields,
        start: portals["AA"].0,
        end: portals["ZZ"].0,
        size: (w, h),
    }
}

#[aoc(day20, part1)]
fn part1(input: &Maze) -> usize {
    let mut current = BTreeMap::new();
    current.insert(input.start, Vec::new());
    let mut visited = BTreeSet::new();
    visited.insert(input.start);
    for n in 0.. {
        assert!(current.len() > 0);
        let mut next = BTreeMap::new();
        for (c, v) in &current {
            let mut v = v.clone();
            v.push(c.clone());
            for &pos in &[
                (c.0 + 1, c.1),
                (c.0 - 1, c.1),
                (c.0, c.1 + 1),
                (c.0, c.1 - 1),
            ] {
                if pos == input.end {
                    //print(input, &v.iter().cloned().collect(), &BTreeMap::new());
                    return n - 1;
                }
                if visited.contains(&pos) {
                    continue;
                }
                visited.insert(pos);
                match input.fields.get(&pos) {
                    Some(Some((p, _))) => {
                        for &p2 in &[
                            (p.0 + 1, p.1),
                            (p.0 - 1, p.1),
                            (p.0, p.1 + 1),
                            (p.0, p.1 - 1),
                        ] {
                            if input.fields.contains_key(&p2) {
                                visited.insert(p2);
                                next.insert(p2, v.clone());
                                break;
                            }
                        }
                    }
                    Some(None) => {
                        next.insert(pos, v.clone());
                    }
                    None => {}
                }
            }
        }
        current = next;
    }
    unreachable!()
}

#[aoc(day20, part2)]
fn part2(input: &Maze) -> usize {
    let mut current = BTreeMap::new();
    current.insert((input.start, 0), Vec::new());
    let mut visited = BTreeSet::new();
    visited.insert((input.start, 0));
    for n in 0.. {
        assert!(current.len() > 0);
        let mut next = BTreeMap::new();
        for (&(c, l), v) in &current {
            let mut v = v.clone();
            v.push(c.clone());
            for &pos in &[
                (c.0 + 1, c.1),
                (c.0 - 1, c.1),
                (c.0, c.1 + 1),
                (c.0, c.1 - 1),
            ] {
                if pos == input.end && l == 0 {
                    return n - 1;
                }
                if visited.contains(&(pos, l)) {
                    continue;
                }
                visited.insert((pos, l));
                match input.fields.get(&pos) {
                    Some(Some((p, d))) => {
                        if l + d <= 0 {
                            for &p2 in &[
                                (p.0 + 1, p.1),
                                (p.0 - 1, p.1),
                                (p.0, p.1 + 1),
                                (p.0, p.1 - 1),
                            ] {
                                if input.fields.contains_key(&p2) {
                                    visited.insert((p2, l + d));
                                    next.insert((p2, l + d), v.clone());
                                    break;
                                }
                            }
                        }
                    }
                    Some(None) => {
                        next.insert((pos, l), v.clone());
                    }
                    None => {}
                }
            }
        }
        current = next;
    }
    unreachable!()
}
