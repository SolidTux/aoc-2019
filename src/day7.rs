use crate::utils::Computer;
use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day7)]
fn ints(input: &str) -> Vec<i128> {
    input
        .split(",")
        .map(|x| x.trim().parse().unwrap())
        .collect()
}

#[aoc(day7, part1)]
fn part1(input: &[i128]) -> i128 {
    let mut data = [0, 1, 2, 3, 4];
    let mut permutations = Vec::new();
    permutohedron::heap_recursive(&mut data, |permutation| {
        permutations.push(permutation.to_vec())
    });
    let mut res = 0;
    for d in permutations {
        let mut c = (0..5)
            .map(|_| Computer::from_code(input))
            .collect::<Vec<_>>();
        let mut output = 0;
        for i in 0..5 {
            output = c[i].run([d[i], output].into_iter().cycle())[0];
        }
        res = res.max(output);
    }
    res
}

#[aoc(day7, part2)]
fn part2(input: &[i128]) -> i128 {
    let mut data = [5, 6, 7, 8, 9];
    let mut permutations = Vec::new();
    permutohedron::heap_recursive(&mut data, |permutation| {
        permutations.push(permutation.to_vec())
    });
    let mut res = 0;
    for d in permutations {
        let mut c = (0..5)
            .map(|_| Computer::from_code(input))
            .collect::<Vec<_>>();
        let mut output = 0;
        for i in 0..5 {
            c[i].input(d[i]);
        }
        'outer: loop {
            for i in 0..5 {
                if !c[i].input(output) {
                    break 'outer;
                }
                if let Some(x) = c[i].output() {
                    output = x;
                } else {
                    break 'outer;
                }
            }
        }
        res = res.max(output);
    }
    res
}
