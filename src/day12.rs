use aoc_runner_derive::{aoc, aoc_generator};
use ndarray::prelude::*;
use regex::Regex;

#[derive(Clone, PartialEq, Eq)]
struct Moon {
    pub pos: Array1<isize>,
    pub v: Array1<isize>,
}

impl Moon {
    pub fn energy(&self) -> isize {
        self.pos.map(|x| x.abs()).sum() * self.v.map(|x| x.abs()).sum()
    }
}

impl std::fmt::Debug for Moon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "pos: {} v: {}", self.pos, self.v)
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct System {
    moons: Vec<Moon>,
}

impl System {
    pub fn step(&mut self) {
        for i in 0..self.moons.len() {
            for j in 0..self.moons.len() {
                let d = (&self.moons[j].pos - &self.moons[i].pos)
                    .map(|&x| x.signum())
                    .to_owned();
                self.moons[i].v += &d;
            }
        }

        for i in 0..self.moons.len() {
            let v = self.moons[i].v.to_owned();
            self.moons[i].pos += &v;
        }
    }

    pub fn energy(&self) -> isize {
        self.moons.iter().map(|x| x.energy()).sum()
    }
}

#[aoc_generator(day12)]
fn parse(input: &str) -> System {
    let re = Regex::new(r"<x=([-\d]\d*), y=([-\d]\d*), z=([-\d]\d*)>").unwrap();
    System {
        moons: re
            .captures_iter(input)
            .map(|x| Moon {
                pos: array![
                    x[1].parse().unwrap(),
                    x[2].parse().unwrap(),
                    x[3].parse().unwrap(),
                ],
                v: array![0, 0, 0],
            })
            .collect(),
    }
}

#[aoc(day12, part1)]
fn part1(input: &System) -> isize {
    let mut s = input.clone();
    for _ in 0..1000 {
        s.step();
    }
    s.energy()
}

#[aoc(day12, part2)]
fn part2(input: &System) -> isize {
    let mut s = input.clone();
    let start = s.clone();
    for n in 1.. {
        s.step();
        if s == start {
            return n;
        }
    }
    unreachable!()
}
